//[SECTION] DECLARING AND ACCESSING ELEMENTS INSIDE AN ARRAY STRUCTURE
 
 //Default/Inital Method in declaring arrays
 let grades = [98, 97, 96, 100, 80, 97, 98, 97, 96, 100, 80];

 //Alternative Way to write down arrays
 let myBootcampTasks = [
	 	'drink HTML',
	 	'eat Bootstrap',
	 	'disect CSS',
	 	'bake GIT',
	 	'sleep with JS'
 ];

 //display the values in the console.
 console.log(myBootcampTasks); 

 //How to know how many elements are there in an array?
 	//check the data type of the variable 'myBootcampTasks'
	console.log(typeof myBootcampTasks); //object
    //length property 
    console.log(myBootcampTasks.length); //5
    console.log(grades.length); 

 //Create a new array structure

 	//this way of declaring arrays is possible
    let mixedData = [29, 'Martin', null, undefined, {}];
    //console.log(mixedData); 
    //NOT RECOMMENDED, take note that when declaring arrays, it should be a 'data-set' of related data.

    let tasks = ['eat', 'sleep', 'wake up']; //3 elements
                 //1       2        3
        //index:   0       1        2
    //CORRECT WAY, it should be a set of 'RELATED DATA'.

//[ACCESSING ELEMENTS OF AN ARRAY]
    console.log(tasks.length);
    console.log(tasks.indexOf('eat')); //0
    console.log(tasks.indexOf('wake up')); //2
    //'index' => refers to the position of an element within the array, you can use indexOf() -> to determine the position of an element according to it's index
   //in array structure keep in mind that the index count starts with '0'.

//Practice Tasks: Access the elements inside the array using it's index count.  
   myBootcampTasks[3] = 'bake cupcakes'; 
   console.log(myBootcampTasks[3]);
   //given that 'drink HTML' is the 1st element it will take the 0 position in the index count.

   //to access/acquire an element within an array use '[]' and pass down the value of the index count. 

   //you can also use this to reassign/update a new value within the array.

   //drink HTML => drink water
   myBootcampTasks[0] = 'drink water';
   console.log(myBootcampTasks[0]);  
   //bake GIT => bake cupcakes
   console.log(myBootcampTasks[3]); 

   //ADDITIONAL NOTES: since we discussed that indexing in an array starts with 0, you can get the length of the array and subtract it by -1, this will allow you to identify the last index of the array. 

   //this is helpful when trying to identify the last index count of an array. 
   let lastElementIndex = myBootcampTasks.length - 1;
   console.log(lastElementIndex); 
   console.log(myBootcampTasks[lastElementIndex]);
  