//[SECTION] Iterator Methods

   // What are Iteration Methods?
   // --> Iteration methods are loops designed to perform repetitive tasks/procedures on array data structures.
   // --> There are very useful for manipulating array data sets resulting in complex tasks.

// forEach()

     //Description:
      //=> similar to a 'for loop' method that will allow to access and iterate each element inside an array. 

     //SYNTAX:
        // arrayName.forEach( function(element) {
        //  statement/procedure
        // })

     //Example:
     let tasks = [
      'eat HTML',
      'drink JS',
      'throw Bootstrap',
      'sleep with React'
     ];

     //NOTE: normally, as a writing convention, when selecting a variable to describe an array or collection it should be in 'PLURAL' form. 

     //it is also a common practive to use the 'singular form' of the array name when describing the content of the collection
     tasks.forEach( function(task) {
      //we can now describe what we want to happen to each element inside the array collection.
      //display all elements one by one in the terminal. 
      console.log(`THIS IS MY TASK ${task}`); 
      //it will upon the use case on how you want to return the elements that will be iterated from the array collection. 
     });

     //Example: use forEach with a conditional statement. 

     const jobs = ['eat', 'pray', 'sleep', 'wine', 'dine'];

     //Lets filter the collection and only accept jobs that are more than 3 letters. 

     const acceptedJobs = []; 

     jobs.forEach( function(job) {
        //state the procedure of what you want to happen in order to accomplish the task. 
        if (job.length > 3) {
         //accept the task
         //what mutator can you use to add an element inside an array?
         acceptedJobs.push(job);
        }
     });

     console.log(jobs); 
     console.log('Here are the list of Accepted Jobs:');
     console.log(acceptedJobs); 

     //map() -> this function access and iterates each element inside an array and returns a NEW ARRAY with different values depending on the function's procedure. 

     //SYNTAX: 
         // let/const resultArray = arrayName.map( function(arrayElement) {
         //    statement
         // })

     //EXAMPLE: 
       let numbers = [1, 2, 3, 4, 5]; 

       //using the map method lets iterate each element inside the array and multiply it by itself and store inside a new variable.

       //for better use case of the map() -> when you to execute a certain procedure on the elements of an array and directly store its outcome into a new array. 
       let mapOutcome = jobs.map( function(job) {
         return job; //the sum of the operation will become the designated return   
       }); 


       console.log('Result of the Map method'); 
       console.log(mapOutcome); 
       console.log(numbers); 

